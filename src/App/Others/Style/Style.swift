//
//  Style.swift
//  Drink
//
//  Created by José Manuel Velázquez on 17/12/18.
//  Copyright © 2018 José Manuel Velázquez. All rights reserved.
//

import UIKit
import SwifterSwift

class Style: NSObject {

}

extension UIView {
    func loadCornerRadius(_ radius: Float) {
        layer.cornerRadius = CGFloat(radius)
    }
    
    func loadShadowBottom() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowRadius = 3
        layer.masksToBounds = false
    }
}

extension UITextField {
    
}

extension UILabel {
    
    func loadStyleLabelFontBold() {
        font = UIFont(name: FontFamily.OpenSans.bold.name, size: font.pointSize)
    }
    
    func loadStyleLabelFontRegular() {
        font = UIFont(name: FontFamily.OpenSans.regular.name, size: font.pointSize)
    }
}

extension UIButton {
    
}

extension UINavigationController {
    
    fileprivate func colorForNavigation(_ color: UIColor) {
        navigationBar.barTintColor = color
        navigationBar.tintColor = color
        view.backgroundColor = color
        navigationBar.isTranslucent = false
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
    }
    
    func loadColorTitleNavigation(_ color: UIColor) {
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
    }
    
    func loadStyleTransparentNavigation() {
        colorForNavigation(.clear)
    }
    
    func loadStyleNavigationWithColor(_ color: UIColor) {
        colorForNavigation(color)
    }
}
